# NOTES.md
Why can't I make the project publically visible? Settings > General > Visibility, project features, permissions
has a pull-down menu with the entries
- Private

and presumably others, but I cannot choose anything there. Adding a LICENSE did not help.
After making the group "temporary-group" public, the menu for project access still did not work,
but after signing out and in it did and now the project "test" is visible without requiring logging in.
