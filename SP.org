# SP.or - scratchpad
* Does GitLab support org-mode?
  It does to some extent, but I am not sure whether this not due to overlap with [[https://en.wikipedia.org/wiki/Markdown][Markdown]] syntax.  

  See [[https://commonmark.org/help/][CommonMark help]] and the [[https://spec.commonmark.org/][official CommonMark specification]].

  [[https://hyperpolyglot.org/lightweight-markup]] has a nice comparison of several /lightweight markup languages/.

  There is no preview function as for ~Markdown~.

** Try some markup
   /Italic/, *bold*, ~code~ are working. So are greek letters like \gamma, \iota and \tau.

   What about _underlined_ or +struck out+ text, operators like \leq, ?

   Numbered lists are working. What about nested lists?
   1. Working:
     * /Italic/, 
     * *bold*, 
     * ~code~,
     * +strikethrough+
     * greek letters \pi and words \tau{}\epsilon{}\chi{}, \gamma{}\iota{}\tau{}. ~{}~ is needed between adjacent characters in words.
     * command lines (~:~ prefix)
     * tables
     * (nested) numbered and bulleted lists
     * mathematical symbols such as \in, \infty, \int, \sum, \partial, \exists, \cdot, \times, \to, \forall, \neg, \land, \lor, \oplus, \pm

     * sub- and superscripts (enclosed in ~{}~): CO_{2}, I^{2}C
    * horizontal rule ~-----~
   2. Not working: 
     * line breaks by ~SPC SPC \n~ or ~\\~
     * _underlined_ text
     * mathematical symbols such as \leq, \geq, \wp, \ldots, \all, \mapsto, \every, \mp
     * verse block
     * formatting not supported by org-mode such as
       - HTML entities such as ~&aleph;~: &aleph; &copy;
       - definition lists
   3. partially working
     * block quotes /(line breaks are not respected, should they?)/
     * internal links (links to files in current project instead of headings)
     * center blocks do not center
   4. To be tested
     * images

   A command line can be written by using ~:~ prefix:
: ls -l

#+begin_quote
  The pandoc extension supports definition lists using
  ~:~ followed by 4 ~SPC~ characters.
  Note that I expect a line break after /using/
#+end_quote
  Try the same text as /example/
#+begin_example
  The pandoc extension supports definition lists using
  ~:~ followed by 4 ~SPC~ characters.
  Note that I expect a line break after /using/
#+end_example
  There are also ~verse~ 
#+begin_verse
  There was a man in Limerick,
  he could rhyme, oh so quick!
#+end_verse
  and ~center~ constructs. The former appears to hide its contents.
  
  This is from the org-mode manual
#+BEGIN_CENTER
  Everything should be made as simple as possible, \\
  but not any simpler
#+END_CENTER
  but it does not appear centered.
-----
   Sub- and superscripts as in CO_2 and E=m·c^2 do not work, I have tried both single and double ~$$~ markers. Enclosing them in ~{}~ helps: CO_{2} and E=m·c^{2}.

#+begin_src common-lisp
   ;; one of the canonical examples
   (defun ! (n)
     "Factorial of non-negative integer N."
     (if (< n 2) 
         (* n (! (- n 1)))))
#+end_src
   Code is rendered as monospaced font.

   $\LaTeX$ formulas are not rendered, but sub- and superscripts are when enclosed in ~{}~.
   
   $$\Gamma(z) = \int_{0}^{\infty} t^{z-1} e^{-t} dt$$  
   
   $$x_{1} = \Gamma(0.5) = \sqrt(\pi)$$  
   
   Try an internal links: [[Next steps in GitLab]] does not work, but what about [[README.md]]?

   What about tables? The ordering of the letters was taken from [[http://www.math.utah.edu/~beebe/greek.html]].  
   /val/ is the value when the letters were used to describe numbers by ancient greeks,
   /Latin/ the corresponding (according to whom?) letter in the latin alphabet and ~EurKEY~
   is the key used in the EurKEY [1,2] keyboard layout after the ~AltGr m~ prefix to enter the
   greek letter.

     | Name    | uc | lc | val | Latin | EurKEY |
     |---------+----+----+-----+-------+--------|
     | Alpha   | Α  | α  |   1 | a     | a      |
     | Beta    | Β  | β  |   2 | b     | b,v    |
     | Gamma   | Γ  | γ  |   3 | c     | g      |
     | Delta   | Δ  | δ  |   4 | d     | d      |
     | Epsilon | Ε  | ε  |   5 | e     | e      |
     | Zeta    | Ζ  | ζ  |   6 | f     | z      |
     | Eta     | Η  | η  |   7 | g     | i      |
     | Theta   | Θ  | θ  |   8 | h     | h      |
     | Iota    | Ι  | ι  |   9 | i¹    | j      |
     | Kappa   | Κ  | κ  |  10 | k     | k      |
     | Lambda  | Λ  | λ  |  20 | l     | l      |
     | Mu      | Μ  | μ  |  30 | m     | m      |
     | Nu      | Ν  | ν  |  40 | n     | n      |
     | Xi      | Ξ  | ξ  |  50 | ²     | x      |
     | Omicron | Ο  | ο  |  60 | o     | o      |
     | Pi      | Π  | π  |  70 | p³    | p      |
     | Rho     | Ρ  | ρ  |  80 | r     | r      |
     | Sigma   | Σ  | σ  |  90 | s     | s      |
     | Tau     | Τ  | τ  | 100 | t     | t      |
     | Upsilon | Υ  | υ  | 200 | u     | y      |
     | Phi     | Φ  | φ  | 300 | v     | f      |
     | Chi     | Χ  | χ  | 400 | w     | c      |
     | Psi     | Ψ  | ψ  | 500 | x⁴    | w      |
     | Omega   | Ω  | ω  | 600 | z     | u      |

   The line breaks are not respected, so I (ab)use a list for these footnotes.
   - ¹ no j  
   - ² ?  
   - ³ no q
   - ⁴ no y
   - [1] [[https://en.wikipedia.org/wiki/EurKEY]]  
   - [2] [[https://eurkey.steffen.bruentjen.eu/][EurKEY website]]
  Try using ~\\~ to mark line breaks instead inside a ~center~ block
  #+begin_center
  ¹ no j\\
  ² ?\\
  ³ no q\\
  ⁴ no y\\
  [1] [[https://en.wikipedia.org/wiki/EurKEY]]\\
  [2] [[https://eurkey.steffen.bruentjen.eu/][EurKEY website]]\\
  #+end_center
  Did this work?

*** TODO [5/6] Checklist for org-mode rendering
    - [x] Does GitLab support rendering of .org files? *No*
    - [x] Are $\LaTeX$ formulas rendered correctly? *No*
    - [x] Tables *Yes*
    - [x] Hyperlinks: URL in doubled ~[]~ are rendered as hyperlinks. *Yes*
    - [x] Line breaks? *No*
    - [~] More formatting examples

* Polynomial roots
** Example from ~scratchpad 2021~
   the roots of x^3 + x + 177 are (according to Emacs Calc, using Symbolic and Fraction modes)
#+begin_example
                       531  ___
                       --- V 3
                        2
                arccos(--------) - 2 pi*(n1 + 1)
        2                 i                        ___
    x = - i cos(--------------------------------) V 3
        3                      3
#+end_example
   for n1 \in {0, 1, 2} we get

   x0 = -5.55530638359

   x1 = (2.77765319185, -4.91386525674)

   x2 = (2.77765319179,  4.91386525677)

   reconstrucing the polynomial from (x - x0) (x - x1) (x - x2) yields
#+begin_example
           3                     2
   p(x) = x  + (-5e-11, -3e-11) x  + (0.9999999996, 3e-10) x + (177., 2.22212255344e-9)
#+end_example

* TODO [1/4] Next steps in GitLab
- [ ] Move the  project to another group. See [[https://docs.gitlab.com/ee/tutorials/move_personal_project_to_a_group.html]].
- [X] Test workflow with local  repository.
- [ ] Create public project(s)
   * ulimyhmpqs
   * datoura
- [~] Migrate some of my notes, starting with small parts of
   * 
   * 
- [ ] Try the wiki
